ECHO OFF
CLS
:MENU
ECHO.
ECHO ...............................................
ECHO =========== Sistem Informasi Akuntansi Bimba AIUEO Ungasan ===========
ECHO ----------------------------------------------------------------------
ECHO Pilih menu berikut untuk melakukan proses installasi aplikasi
ECHO ...............................................
ECHO.
ECHO 1 - Install Applikasi
ECHO 2 - Kosongkan Database
ECHO 3 - Update Aplikasi
ECHO 4 - Start Aplikasi
ECHO 5 - EXIT
ECHO.
SET /P M=Type 1, 2, 3, 4 or 5 then press ENTER:
IF %M%==1 GOTO INST
IF %M%==2 GOTO CLEANDB
IF %M%==3 GOTO UPDT
IF %M%==4 GOTO STRT
IF %M%==5 GOTO EOF
:INST
CLS
ECHO ...............................................
ECHO Extract vendor zip
copy .env.sample .env
unzip -o vendor.zip
ECHO proses migration database
mysql -u root < migration/install.sql
yii migrate
ECHO Finish install.
ECHO ...............................................
GOTO MENU

:CLEANDB
CLS
ECHO ...............................................
ECHO Rebuid Database
mysql -u root < migration/clean_db.sql
ECHO proses migration database
yii migrate
ECHO Finish Cleaning.
ECHO ...............................................
GOTO MENU


:UPDT
CLS
ECHO ...............................................
ECHO Pastikan koneksi internet stabil dan sudah terinstall git pada komputer
ECHO ...............................................
ECHO Download update aplikasi dari server
git pull origin master
ECHO Extract vendor zip terbaru
unzip -o vendor.zip
ECHO proses migration database
yii migrate
ECHO Finish update.
ECHO ...............................................

:STRT
CLS
ECHO ...............................................
ECHO Start Server Aplikasi
ECHO Pastikan halaman ini tetap aktif saat mengakses aplikasi
ECHO Akses aplikasi pada web browser dengan alamat http://loclhost:89
ECHO ...............................................
yii serve --port=89
GOTO MENU

