<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\Akun */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Daftar Akun');
$this->params['breadcrumbs'][] = $this->title;
?>
<?php // echo $this->render('_search', ['model' => $searchModel]); ?>
<div class="akun-index">
    <div class="panel panel-primary">
        <div class="panel-heading"><?= Html::encode($this->title) ?></div>
        <div class="panel-body">
            <div class="row">
                <div class="col-lg-12">
                    <div class="pull-right">
                        <p>User: Nama User Date: <?= date('d M Y')?></p>
                        <?= Html::a(Yii::t('app', 'Buat akun Baru'), ['create'], ['class' => 'btn btn-success']) ?>
                        <?= Html::a(Yii::t('app', 'Buat Jurnal Baru'), ['create-journal'], ['class' => 'btn btn-success']) ?>
                    </div>
                </div>
                <div class="col-lg-12">
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],
                            'kode',
                            'nama',
                            [
                                 'attribute' => 'kelompok_akun_id',
                                 'value' => 'kelompokAkun.nama',
                                'filter' => \app\models\KelompokAkun::selectOptions()
                            ],
                            'pajak',
                            'saldo_awal',
                            'saldo',
                            'keterangan:ntext',

                            ['class' => 'yii\grid\ActionColumn'],
                        ],
                    ]); ?>
                </div>
            </div>
        </div>
    </div>
</div>
