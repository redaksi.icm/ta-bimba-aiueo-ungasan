<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\TbMurid */

$this->title = $model->nim_murid;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Tb Murids'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="tb-murid-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->nim_murid], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->nim_murid], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'nim_murid',
            'nama',
            'kode_kelas',
            'tanggal_lahir',
            'alamat',
            'no_telp',
            'asal_sekolah:ntext',
            'tgl_masuk',
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>
