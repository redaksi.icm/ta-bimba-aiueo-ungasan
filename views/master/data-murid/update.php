<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TbMurid */
$actionName = Yii::t('app', 'Update Tb Murid: {name}', [
    'name' => $model->nim_murid,
])$this->title = Yii::$app->name . ' ' . Yii::t('app', 'Update Tb Murid: {name}', [
    'name' => $model->nim_murid,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Tb Murids'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->nim_murid, 'url' => ['view', 'id' => $model->nim_murid]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="tb-murid-update">

    <?= $this->render('_form', [
        'model' => $model,
        'actionName' => $actionName,
    ]) ?>

</div>
