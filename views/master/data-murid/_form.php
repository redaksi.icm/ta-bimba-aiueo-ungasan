<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TbMurid */
/* @var $form yii\widgets\ActiveForm */

$title = $actionName;
?>

<div class="tb-murid-form">
    <div class="panel panel-primary">
        <div class="panel-heading"><?=  Html::encode($title) ?></div>
        <?php $form = ActiveForm::begin(); ?>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-12">
                    <?= $form->field($model, 'nim_murid')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'nama')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'kode_kelas')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'tanggal_lahir')->textInput() ?>

                    <?= $form->field($model, 'alamat')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'no_telp')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'asal_sekolah')->textarea(['rows' => 6]) ?>

                    <?= $form->field($model, 'tgl_masuk')->textInput() ?>

                    <?= $form->field($model, 'created_at')->textInput() ?>

                    <?= $form->field($model, 'updated_at')->textInput() ?>


                </div>
            </div>
        </div>
        <div class="panel-footer">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
                    </div>
                </div>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>
