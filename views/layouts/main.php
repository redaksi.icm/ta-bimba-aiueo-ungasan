<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
$css = <<<CSS
.bimba-logo{
    margin: 10px !important;
    width: 100px !important;
}

.container {
    width: 95%;
}
CSS;
$this->registerCss($css);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <div class="row">
        <div class="col-md-4">
            <img class="bimba-logo" src="<?= Yii::$app->urlManager->baseUrl?>/images/logo.jpg" alt="" width="100px">
        </div>
        <div class="col-md-8">
            <h1>Sistem Informasi Akuntansi biMBA - AIUEO unit Ungasan</h1>
        </div>
    </div>
    <!--
    <?php
    NavBar::begin([
        'brandLabel' => Yii::$app->name,
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right', 'id' => 'navbar_menu'],
        'items' => [
            ['id' => 'home_menu', 'label' => 'Home', 'url' => ['/site/index']],
            ['id' => 'about_menu', 'label' => 'About', 'url' => ['/site/about']],
            ['id' => 'contact_menu', 'label' => 'Contact', 'url' => ['/site/contact']],
            Yii::$app->user->isGuest ? (
            ['id' => 'login_menu', 'label' => 'Login', 'url' => ['/site/login']]
            ) : (
                '<li>'
                . Html::beginForm(['/site/logout'], 'post')
                . Html::submitButton(
                    'Logout (' . Yii::$app->user->identity->username . ')',
                    ['class' => 'btn btn-link logout']
                )
                . Html::endForm()
                . '</li>'
            )
        ],
    ]);
    NavBar::end();
    ?>
    -->
    <div class="container">
        <?= Alert::widget() ?>
        <div class="row">
            <div class="col-md-3">
                <div class="panel panel-primary">
                    <div class="panel-body">
                        <?= \app\widgets\ListGroupMenu::widget([
                            'id' => 'navbar_menu',
                            'items' => [
                                // Important: you need to specify url as 'controller/action',
                                // not just as 'controller' even if default action is used.
                                ['id' => 'home_menu','label' => 'Dashboard', 'url' => ['site/index']],
                                ['id' => 'laporan_menu','label' => 'Laporan', 'url' => ['/laporan/index']],
                                ['id' => 'transaksi_menu','label' => 'Transaksi', 'url' => ['/transaksi/index']],
                                ['id' => 'biaya_menu','label' => 'Biaya', 'url' => ['/biaya/index']],
                                ['id' => 'murid_menu','label' => 'Data Murid', 'url' => ['/master/data-murid/index']],
                                ['id' => 'pegawai_menu','label' => 'Data Pegawai', 'url' => ['/master/data-pegawai/index']],
                                ['id' => 'payroll_menu','label' => 'Payroll', 'url' => ['/payroll/index']],
                                ['id' => 'daftar_transaksi_menu','label' => 'Daftar Transaksi', 'url' => ['transaksi/index']],
                                ['id' => 'akun_menu','label' => 'Daftar akun', 'url' => ['acc/akun/index']],
                                ['id' => 'user_menu','label' => 'User', 'url' => ['master/user/index']],
                                Yii::$app->user->isGuest ? (
                                ['id' => 'login_menu', 'label' => 'Login', 'url' => ['/site/login']]
                                ) : (
                                    '<li>'
                                    . Html::beginForm(['/site/logout'], 'post')
                                    . Html::submitButton(
                                        'Logout (' . Yii::$app->user->identity->username . ')',
                                        ['class' => 'btn btn-link logout']
                                    )
                                    . Html::endForm()
                                    . '</li>'
                                )
                            ],
                        ])?>
                    </div>
                </div>
            </div>
            <div class="col-md-9">
                <?= $content ?>
            </div>
        </div>

    </div>
</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
