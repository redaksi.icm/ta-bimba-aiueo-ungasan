# Sistem Informasi Akuntansi Bimba AIUEO Ungasan

Tugas Akhir Sistem Informasi Akuntansi BIMBA AIUEO Unit Ungasan

## Kebutuhan Developement
1. Download aplikasi [Git](https://git-scm.com/downloads) untuk keperluan download applikasi
2. Install aplikasi [AnyDesk](https://download.anydesk.com/AnyDesk.exe) untuk keperluan remote bantuan installasi project
3. Install web server lokal [Xampp](https://www.apachefriends.org/xampp-files/7.4.10/xampp-windows-x64-7.4.10-0-VC15-installer.exe)
4. Install Tool Database ([Sqlyog](https://s3.amazonaws.com/SQLyog_Community/SQLyog+13.1.7/SQLyog-13.1.7-0.x64Community.exe), [HeidiSql](https://www.heidisql.com/download.php?download=installer), [Navicat](https://www.navicat.com/en/download/navicat-for-mysql))  pilih salah satu sesuai keinginan atau bisa menggunkan tool database lainnya.

## Cara Install
Terdapat 2 cara untuk mengisntall project pada webserver:

Sebelum melangkah lebih jauh, pastikan server xampp sudah aktif. ketik ```localhost``` pada alamat web browser untuk memastikan webserver telah aktif.
   
1. Dengan cara download [Project zip](https://gitlab.com/rusli-nasir-m/ta-bimba-aiueo-ungasan/-/archive/master/ta-bimba-aiueo-ungasan-master.zip)
   - Extract Zip file pada folder htdocs ```sesuaikan dengan folder dimana installasi xampp contohnya: c:/xampp/htdocs``` 
   - Masuk kedalam folder projek kemudian klik file ```Install.bat``` untuk melakukan proses installasi aplikasi. 
   - Buka browser dan ketikkan url ```localhost/[nama_folder_aplikasi_di_htdocs]```
   
2. Dengan cara menggunakan cmd
    - ```
      #C:/> cd xampp/htdocs
      #C:/> git clone https://gitlab.com/rusli-nasir-m/ta-bimba-aiueo-ungasan.git
      #C:/> cd ta-bimba-aiueo-ungasan
      #C:/> Install.bat
      ```   
    - Buka browser dan ketikkan url ```localhost/[nama_folder_aplikasi_di_htdocs]```

## Struktur Aplikasi

- Akses Kepala Akademik
    - [ ] Manajemen Data User
    - [ ] Manajemen Data Murid
    - [ ] Manajemen Data Pegawai
- Akses Pimpinan
    - [ ] Manajemen Data User
- Akses Staff Keuangan
    - [ ] Transaksi Penerimaan 
- Akses Financial Account
    - [ ] Transaksi Pengeluaran
    - [ ] Komponen Gaji
    - [ ] Payroll
        - [ ] Cetak slip gaji
- Akses Accounting
    - [ ] Daftar Akun
    - [ ] Jurnal Umum
        - [ ] Input Jurnal Umum
    - [ ] Asset
        - [ ] Input Asset
    - [ ] Hutang
        - [ ] Pembayaran Hutang
    - [ ] Piutang
        - [ ] Penagihan Piutang
    - [ ] Laporan Keuangan
        - [ ] Laporan Hutang
        - [ ] Laporan Piutang
        - [ ] Laporan Perubahan Modal
        - [ ] Laporan Arus Kas
        - [ ] Laporan Neraca
        - [ ] Laporan Laba Rugi
        - [ ] Laporan Kas & Bank
    
