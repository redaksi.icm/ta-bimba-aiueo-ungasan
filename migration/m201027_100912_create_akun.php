<?php

use yii\db\Schema;
use yii\db\Migration;

class m201027_100912_create_akun extends Migration
{

    public function init()
    {
        $this->db = 'db';
        parent::init();
    }

    public function safeUp()
    {
        $tableOptions = 'ENGINE=InnoDB';

        $this->createTable('{{%kelompok_akun}}',[
            'id'=> $this->primaryKey(3)->unsigned(),
            'nama'=> $this->char(10)->notNull()->defaultValue(''),
        ], $tableOptions);


        $this->createTable('{{%tb_akun}}',[
            'id'=> $this->primaryKey(5)->unsigned(),
            'nama'=> $this->string(30)->notNull()->defaultValue(''),
            'kode'=> $this->string(5)->notNull()->defaultValue(''),
            'kelompok_akun_id'=> $this->integer(3)->unsigned()->notNull(),
            'pajak'=> $this->tinyInteger(1)->notNull()->defaultValue(1),
            'saldo_awal'=> $this->bigInteger(20)->notNull()->defaultValue(0),
            'saldo'=> $this->bigInteger(20)->notNull()->defaultValue(0),
            'keterangan'=> $this->text()->notNull(),
        ], $tableOptions);

        $this->createIndex('idx_kelompok_akun_id','{{%tb_akun}}',['kelompok_akun_id'],false);

        $this->addForeignKey(
            'fk_tb_akun_kelompok_akun_id',
            '{{%tb_akun}}', 'kelompok_akun_id',
            '{{%kelompok_akun}}', 'id',
            'CASCADE', 'CASCADE'
        );
    }

    public function safeDown()
    {
            $this->dropForeignKey('fk_tb_akun_kelompok_akun_id', '{{%tb_akun}}');
            $this->dropTable('{{%kelompok_akun}}');
            $this->dropTable('{{%tb_akun}}');
    }
}
