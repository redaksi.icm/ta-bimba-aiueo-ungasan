<?php

use yii\db\Schema;
use yii\db\Migration;

class m201027_101129_kelompok_akunDataInsert extends Migration
{

    public function init()
    {
        $this->db = 'db';
        parent::init();
    }

    public function safeUp()
    {
        $this->batchInsert('{{%kelompok_akun}}',
                           ["id", "nama"],
                            [
    [
        'id' => '1',
        'nama' => 'Aktiva',
    ],
    [
        'id' => '2',
        'nama' => 'Kewajiban',
    ],
    [
        'id' => '3',
        'nama' => 'Modal',
    ],
    [
        'id' => '4',
        'nama' => 'Pendapatan',
    ],
    [
        'id' => '5',
        'nama' => 'Biaya',
    ],
]
        );
    }

    public function safeDown()
    {
        //$this->truncateTable('{{%kelompok_akun}} CASCADE');
    }
}
