<?php

use yii\db\Schema;
use yii\db\Migration;

class m201027_100913_create_table_lainnnya extends Migration
{

    public function init()
    {
        $this->db = 'db';
        parent::init();
    }

    public function safeUp()
    {
        $tableOptions = 'ENGINE=InnoDB';

        $this->createTable('{{%pegawai}}',[
            'id_pegawai'=> $this->string(100)->notNull(),
            'nama_pegawai'=> $this->string(50)->null()->defaultValue(null),
            'jenis_kelamin'=> $this->string(20)->null()->defaultValue(null),
            'golongan'=> $this->string(25)->null()->defaultValue(null),
            'jabatan'=> $this->string(25)->null()->defaultValue(null),
        ], $tableOptions);

        $this->addPrimaryKey('pk_on_pegawai','{{%pegawai}}',['id_pegawai']);

        $this->createTable('{{%tb_gaji}}',[
            'id_gaji'=> $this->string(100)->notNull(),
            'id_pegawai'=> $this->string(100)->null()->defaultValue(null),
            'tanggal'=> $this->date()->null()->defaultValue(null),
            'masa_kerja'=> $this->string(50)->null()->defaultValue(null),
            'tunjangan_makan'=> $this->integer(11)->null()->defaultValue(null),
            'tunjangan_kesehatan'=> $this->integer(11)->null()->defaultValue(null),
            'jabatan'=> $this->string(25)->null()->defaultValue(null),
            'gaji_pokok'=> $this->integer(11)->null()->defaultValue(null),
            'potongan'=> $this->integer(11)->null()->defaultValue(null),
            'total_gaji'=> $this->integer(11)->null()->defaultValue(null),
        ], $tableOptions);

        $this->addPrimaryKey('pk_on_tb_gaji','{{%tb_gaji}}',['id_gaji']);

        $this->createTable('{{%tb_kas_masuk}}',[
            'id_kas_masuk'=> $this->string(100)->null()->defaultValue(null),
            'kode_penerimaan'=> $this->string(100)->null()->defaultValue(null),
            'tanggal_penerimaan'=> $this->date()->null()->defaultValue(null),
            'jumlah'=> $this->decimal(15, 4)->null()->defaultValue(null),
            'created_at'=> $this->datetime()->null()->defaultValue(null),
            'updated_at'=> $this->datetime()->null()->defaultValue(null),
        ], $tableOptions);


        $this->createTable('{{%tb_murid}}',[
            'nim_murid'=> $this->string(100)->notNull(),
            'nama'=> $this->string(255)->null()->defaultValue(null),
            'kode_kelas'=> $this->string(255)->null()->defaultValue(null),
            'tanggal_lahir'=> $this->date()->null()->defaultValue(null),
            'alamat'=> $this->string(255)->null()->defaultValue(null),
            'no_telp'=> $this->string(200)->null()->defaultValue(null),
            'asal_sekolah'=> $this->text()->null()->defaultValue(null),
            'tgl_masuk'=> $this->date()->null()->defaultValue(null),
            'created_at'=> $this->datetime()->null()->defaultValue(null),
            'updated_at'=> $this->datetime()->null()->defaultValue(null),
        ], $tableOptions);

        $this->addPrimaryKey('pk_on_tb_murid','{{%tb_murid}}',['nim_murid']);

        $this->createTable('{{%tb_pengajuan}}',[
            'id_pengajuan'=> $this->string(100)->notNull(),
            'id_pegawai'=> $this->string(100)->null()->defaultValue(null),
            'jenis_pengajuan'=> $this->string(25)->null()->defaultValue(null),
            'kegiatan'=> $this->string(100)->null()->defaultValue(null),
        ], $tableOptions);

        $this->addPrimaryKey('pk_on_tb_pengajuan','{{%tb_pengajuan}}',['id_pengajuan']);

        $this->createTable('{{%tb_pengeluaran}}',[
            'id_pengeluaran'=> $this->string(100)->notNull(),
            'id_user'=> $this->integer(11)->null()->defaultValue(null),
            'id_pengajuan'=> $this->string(100)->null()->defaultValue(null),
            'jenis_pengeluaran'=> $this->string(25)->null()->defaultValue(null),
            'tanggal_realisasi'=> $this->date()->null()->defaultValue(null),
            'jumlah_pengeluaran'=> $this->integer(11)->null()->defaultValue(null),
        ], $tableOptions);

        $this->addPrimaryKey('pk_on_tb_pengeluaran','{{%tb_pengeluaran}}',['id_pengeluaran']);

        $this->createTable('{{%tb_piutang}}',[
            'no_transaksi'=> $this->string(100)->null()->defaultValue(null),
            'nim_siswa'=> $this->string(255)->null()->defaultValue(null),
            'tanggal_transaksi'=> $this->date()->null()->defaultValue(null),
            'tanggal_jatuh_tempo'=> $this->date()->null()->defaultValue(null),
            'total'=> $this->decimal(15, 4)->null()->defaultValue(null),
            'created_at'=> $this->datetime()->null()->defaultValue(null),
            'updated_at'=> $this->datetime()->null()->defaultValue(null),
            'import_at'=> $this->datetime()->null()->defaultValue(null),
            'total_bayar'=> $this->decimal(15, 4)->null()->defaultValue(null),
            'sisa_piutang'=> $this->decimal(15, 4)->null()->defaultValue(null),
        ], $tableOptions);
    }

    public function safeDown()
    {
        $this->dropPrimaryKey('pk_on_pegawai','{{%pegawai}}');
        $this->dropTable('{{%pegawai}}');
            $this->dropPrimaryKey('pk_on_tb_gaji','{{%tb_gaji}}');
            $this->dropTable('{{%tb_gaji}}');
            $this->dropTable('{{%tb_kas_masuk}}');
            $this->dropPrimaryKey('pk_on_tb_murid','{{%tb_murid}}');
            $this->dropTable('{{%tb_murid}}');
            $this->dropPrimaryKey('pk_on_tb_pengajuan','{{%tb_pengajuan}}');
            $this->dropTable('{{%tb_pengajuan}}');
            $this->dropPrimaryKey('pk_on_tb_pengeluaran','{{%tb_pengeluaran}}');
            $this->dropTable('{{%tb_pengeluaran}}');
            $this->dropTable('{{%tb_piutang}}');
    }
}
