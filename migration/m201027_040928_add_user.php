<?php

use app\models\User;
use yii\db\Migration;

/**
 * Class m201027_040928_add_user
 */
class m201027_040928_add_user extends Migration
{
    private $userTable = '{{%user}}';
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $defaultPass = Yii::$app->security->generatePasswordHash('123456');

        $this->batchInsert($this->userTable,['username','password_hash','password_raw','status','role','created_at','updated_at'],[
            ['kepala_akademik',$defaultPass,'123456',User::STATUS_ACTIVE,User::ROLE_KEPALA_AKADEMIK,time(),time()],
            ['pimpinan',$defaultPass,'123456',User::STATUS_ACTIVE,User::ROLE_PIMPINAN,time(),time()],
            ['accounting',$defaultPass,'123456',User::STATUS_ACTIVE,User::ROLE_ACCOUNTING,time(),time()],
            ['finacc',$defaultPass,'123456',User::STATUS_ACTIVE,User::ROLE_FINANCIAL_ACCOUNT,time(),time()],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201027_040928_add_user cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201027_040928_add_user cannot be reverted.\n";

        return false;
    }
    */
}
