<?php

use yii\db\Schema;
use yii\db\Migration;

class m201027_101310_tb_akunDataInsert extends Migration
{

    public function init()
    {
        $this->db = 'db';
        parent::init();
    }

    public function safeUp()
    {
        $this->batchInsert('{{%tb_akun}}',
                           ["id", "nama", "kode", "kelompok_akun_id", "pajak", "saldo_awal", "saldo", "keterangan"],
                            [
    [
        'id' => '2',
        'nama' => 'Kas',
        'kode' => '101',
        'kelompok_akun_id' => '1',
        'pajak' => '1',
        'saldo_awal' => '0',
        'saldo' => '0',
        'keterangan' => 'aktiva lancar',
    ],
    [
        'id' => '3',
        'nama' => 'Sewa Dibayar Dimuka',
        'kode' => '102',
        'kelompok_akun_id' => '1',
        'pajak' => '1',
        'saldo_awal' => '0',
        'saldo' => '0',
        'keterangan' => 'aktiva',
    ],
    [
        'id' => '4',
        'nama' => 'Perlengkapan',
        'kode' => '103',
        'kelompok_akun_id' => '1',
        'pajak' => '1',
        'saldo_awal' => '0',
        'saldo' => '0',
        'keterangan' => 'aktiva',
    ],
    [
        'id' => '5',
        'nama' => 'Peralatan',
        'kode' => '104',
        'kelompok_akun_id' => '1',
        'pajak' => '1',
        'saldo_awal' => '0',
        'saldo' => '0',
        'keterangan' => 'aktiva',
    ],
    [
        'id' => '6',
        'nama' => 'Akumulasi Penyusutan Peralatan',
        'kode' => '105',
        'kelompok_akun_id' => '1',
        'pajak' => '1',
        'saldo_awal' => '0',
        'saldo' => '0',
        'keterangan' => 'aktiva',
    ],
    [
        'id' => '7',
        'nama' => 'Modal',
        'kode' => '301',
        'kelompok_akun_id' => '3',
        'pajak' => '1',
        'saldo_awal' => '0',
        'saldo' => '0',
        'keterangan' => 'modal',
    ],
    [
        'id' => '8',
        'nama' => 'Pendapatan Proyek',
        'kode' => '401',
        'kelompok_akun_id' => '4',
        'pajak' => '1',
        'saldo_awal' => '0',
        'saldo' => '0',
        'keterangan' => 'pendapatan',
    ],
    [
        'id' => '9',
        'nama' => 'Beban Gaji',
        'kode' => '501',
        'kelompok_akun_id' => '5',
        'pajak' => '1',
        'saldo_awal' => '0',
        'saldo' => '0',
        'keterangan' => 'biaya',
    ],
    [
        'id' => '10',
        'nama' => 'Beban Upah',
        'kode' => '502',
        'kelompok_akun_id' => '5',
        'pajak' => '1',
        'saldo_awal' => '0',
        'saldo' => '0',
        'keterangan' => 'biaya',
    ],
    [
        'id' => '11',
        'nama' => 'Beban Transportasi',
        'kode' => '503',
        'kelompok_akun_id' => '5',
        'pajak' => '1',
        'saldo_awal' => '0',
        'saldo' => '0',
        'keterangan' => 'biaya',
    ],
    [
        'id' => '12',
        'nama' => 'Beban Konsumsi',
        'kode' => '504',
        'kelompok_akun_id' => '5',
        'pajak' => '1',
        'saldo_awal' => '0',
        'saldo' => '0',
        'keterangan' => 'biaya
',
    ],
    [
        'id' => '13',
        'nama' => 'Beban Telpon',
        'kode' => '505',
        'kelompok_akun_id' => '5',
        'pajak' => '1',
        'saldo_awal' => '0',
        'saldo' => '0',
        'keterangan' => 'biaya',
    ],
    [
        'id' => '14',
        'nama' => 'Beban Internet',
        'kode' => '506',
        'kelompok_akun_id' => '5',
        'pajak' => '1',
        'saldo_awal' => '0',
        'saldo' => '0',
        'keterangan' => 'biaya',
    ],
    [
        'id' => '15',
        'nama' => 'Beban Sewa',
        'kode' => '507',
        'kelompok_akun_id' => '5',
        'pajak' => '1',
        'saldo_awal' => '0',
        'saldo' => '0',
        'keterangan' => 'biaya',
    ],
    [
        'id' => '16',
        'nama' => 'Beban Perlengkapan',
        'kode' => '508',
        'kelompok_akun_id' => '5',
        'pajak' => '1',
        'saldo_awal' => '0',
        'saldo' => '0',
        'keterangan' => 'biaya',
    ],
    [
        'id' => '17',
        'nama' => 'Ikhtisar Laba Rugi',
        'kode' => '302',
        'kelompok_akun_id' => '3',
        'pajak' => '0',
        'saldo_awal' => '0',
        'saldo' => '0',
        'keterangan' => 'Ikhtisar Laba Rugi',
    ],
    [
        'id' => '18',
        'nama' => 'Bank Nasional',
        'kode' => '106',
        'kelompok_akun_id' => '1',
        'pajak' => '1',
        'saldo_awal' => '0',
        'saldo' => '0',
        'keterangan' => 'Akun Bank Nasional',
    ],
]
        );
    }

    public function safeDown()
    {
        //$this->truncateTable('{{%tb_akun}} CASCADE');
    }
}
