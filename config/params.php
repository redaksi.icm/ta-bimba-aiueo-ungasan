<?php

return [
    'adminEmail' => 'admin@example.com',
    'senderEmail' => 'noreply@example.com',
    'senderName' => 'Example.com mailer',
    'user_access' => [
        10 => 'Kepala Akademik',
        20 => 'Pimpinan',
        30 => 'Financial Account',
        40 => 'Accounting',
    ]
];
