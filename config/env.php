<?php
/**
 * Require helpers
 */

use Dotenv\Dotenv;

require_once(__DIR__ . '/helpers.php');

/**
 * Load application environment from .env file
 */
$dotenv = new Dotenv(dirname(__DIR__));
$dotenv->load();

/**
 * Init application constants
 */
defined('YII_DEBUG') or define('YII_DEBUG', dotenv('YII_DEBUG'));
defined('YII_ENV') or define('YII_ENV', dotenv('YII_ENV', 'prod'));
