<?php
/**
 * Yii2 Shortcuts
 * @author Eugene Terentev <eugene@terentev.net>
 * -----
 * This file is just an example and a place where you can add your own shortcuts,
 * it doesn't pretend to be a full list of available possibilities
 * -----
 */

use yii\helpers\FileHelper;

/**
 * @return int|string
 */
function getMyId()
{
    return Yii::$app->user->getId();
}

/**
 * @param string $view
 * @param array $params
 * @return string
 */
function render($view, $params = [])
{
    return Yii::$app->controller->render($view, $params);
}

/**
 * @param $url
 * @param int $statusCode
 * @return \yii\web\Response
 */
function redirect($url, $statusCode = 302)
{
    return Yii::$app->controller->redirect($url, $statusCode);
}

/**
 * @param $form \yii\widgets\ActiveForm
 * @param $model
 * @param $attribute
 * @param array $inputOptions
 * @param array $fieldOptions
 * @return string
 */
function activeTextinput($form, $model, $attribute, $inputOptions = [], $fieldOptions = [])
{
    return $form->field($model, $attribute, $fieldOptions)->textInput($inputOptions);
}

/**
 * @param string $key
 * @param mixed $default
 * @return mixed
 */
function dotenv($key, $default = null) {

    $value = getenv($key);

    if ($value === false) {
        return $default;
    }

    switch (strtolower($value)) {
        case 'true':
        case '(true)':
            return true;

        case 'false':
        case '(false)':
            return false;
    }

    return $value;
}

function setDotEnvVar($name, $value)
{

    $path = Yii::getAlias('@app/.env');

    if (!file_exists($path)) {
        throw new Exception("No .env file exists at {$path}");
    }

    $contents = file_get_contents($path);
    $qName = preg_quote($name, '/');
    $slashedValue = addslashes($value);
    // Only surround with quotes if the value contains a space
    if (strpos($slashedValue, ' ') !== false || strpos($slashedValue, '#') !== false) {
        $slashedValue = "\"$slashedValue\"";
    }
    $qValue = str_replace('$', '\\$', $slashedValue);
    $contents = preg_replace("/^(\s*){$qName}=.*/m", "\$1$name=$qValue", $contents, -1, $count);

    if ($count === 0) {
        $contents = rtrim($contents);
        $contents = ($contents ? $contents . PHP_EOL . PHP_EOL : '') . "$name=$slashedValue" . PHP_EOL;
    }

    writeToFile($path, $contents);

    // Now actually set the environment variable
    putenv("{$name}={$value}");
}


/**
 * @param $file
 * @param $contents
 * @param array $options
 * @throws ErrorException
 * @throws \yii\base\Exception
 */
function writeToFile($file, $contents, $options = [])
{
    $file = FileHelper::normalizePath($file);
    $dir = dirname($file);

    if (!is_dir($dir)) {
        if (!isset($options['createDirs']) || $options['createDirs']) {
            FileHelper::createDirectory($dir);
        } else {
            throw new InvalidArgumentException("Cannot write to \"{$file}\" because the parent directory doesn't exist.");
        }
    }

//    if (isset($options['lock'])) {
//        $lock = (bool)$options['lock'];
//    } else {
//        $lock = FileHelper::useFileLocks();
//    }

//    if ($lock) {
//        $mutex = Craft::$app->getMutex();
//        $lockName = md5($file);
//        if (!$mutex->acquire($lockName, 2)) {
//            throw new ErrorException("Unable to acquire a lock for file \"{$file}\".");
//        }
//    } else {
//        $lockName = $mutex = null;
//    }

    $flags = 0;
    if (!empty($options['append'])) {
        $flags |= FILE_APPEND;
    }

    if (file_put_contents($file, $contents, $flags) === false) {
        throw new ErrorException("Unable to write new contents to \"{$file}\".");
    }

    // Invalidate opcache
//    FileHelper::invalidate($file);

//    if ($lock) {
//        $mutex->release($lockName);
//    }
}

function getParams($key){
    return \yii\helpers\ArrayHelper::getValue(Yii::$app->params,$key);
}

function getParamsValue($key, $subKey){
    $induk = getParams($key);
    return \yii\helpers\ArrayHelper::getValue($induk,$subKey);
}
