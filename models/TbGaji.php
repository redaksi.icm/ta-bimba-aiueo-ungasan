<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%tb_gaji}}".
 *
 * @property string $id_gaji
 * @property string|null $id_pegawai
 * @property string|null $tanggal
 * @property string|null $masa_kerja
 * @property int|null $tunjangan_makan
 * @property int|null $tunjangan_kesehatan
 * @property string|null $jabatan
 * @property int|null $gaji_pokok
 * @property int|null $potongan
 * @property int|null $total_gaji
 */
class TbGaji extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%tb_gaji}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_gaji'], 'required'],
            [['tanggal'], 'safe'],
            [['tunjangan_makan', 'tunjangan_kesehatan', 'gaji_pokok', 'potongan', 'total_gaji'], 'integer'],
            [['id_gaji', 'id_pegawai'], 'string', 'max' => 100],
            [['masa_kerja'], 'string', 'max' => 50],
            [['jabatan'], 'string', 'max' => 25],
            [['id_gaji'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_gaji' => Yii::t('app', 'Id Gaji'),
            'id_pegawai' => Yii::t('app', 'Id Pegawai'),
            'tanggal' => Yii::t('app', 'Tanggal'),
            'masa_kerja' => Yii::t('app', 'Masa Kerja'),
            'tunjangan_makan' => Yii::t('app', 'Tunjangan Makan'),
            'tunjangan_kesehatan' => Yii::t('app', 'Tunjangan Kesehatan'),
            'jabatan' => Yii::t('app', 'Jabatan'),
            'gaji_pokok' => Yii::t('app', 'Gaji Pokok'),
            'potongan' => Yii::t('app', 'Potongan'),
            'total_gaji' => Yii::t('app', 'Total Gaji'),
        ];
    }

    /**
     * {@inheritdoc}
     * @return \app\models\query\TbGajiQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\query\TbGajiQuery(get_called_class());
    }
}
