<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%tb_akun}}".
 *
 * @property int $id
 * @property string $nama
 * @property string $kode
 * @property int $kelompok_akun_id
 * @property int $pajak
 * @property int $saldo_awal
 * @property int $saldo
 * @property string $keterangan
 *
 * @property JurnalDetail[] $jurnalDetails
 * @property KelompokAkun $kelompokAkun
 */
class Akun extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%tb_akun}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kelompok_akun_id', 'keterangan'], 'required'],
            [['kelompok_akun_id', 'pajak', 'saldo_awal', 'saldo'], 'integer'],
            [['keterangan'], 'string'],
            [['nama'], 'string', 'max' => 30],
            [['kode'], 'string', 'max' => 5],
            [['kelompok_akun_id'], 'exist', 'skipOnError' => true, 'targetClass' => KelompokAkun::className(), 'targetAttribute' => ['kelompok_akun_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'nama' => Yii::t('app', 'Nama Akun'),
            'kode' => Yii::t('app', 'Kode Akun'),
            'kelompok_akun_id' => Yii::t('app', 'Kategori Akun'),
            'pajak' => Yii::t('app', 'Pajak'),
            'saldo_awal' => Yii::t('app', 'Saldo Awal'),
            'saldo' => Yii::t('app', 'Saldo'),
            'keterangan' => Yii::t('app', 'Keterangan'),
            'kelompokAkun' => Yii::t('app', 'Kategori Akun'),
        ];
    }

    /**
     * Gets query for [[JurnalDetails]].
     *
     * @return \yii\db\ActiveQuery|\app\models\query\JurnalDetailQuery
     */
    public function getJurnalDetails()
    {
        return $this->hasMany(JurnalDetail::className(), ['akun_id' => 'id']);
    }

    /**
     * Gets query for [[KelompokAkun]].
     *
     * @return \yii\db\ActiveQuery|\app\models\query\KelompokAkunQuery
     */
    public function getKelompokAkun()
    {
        return $this->hasOne(KelompokAkun::className(), ['id' => 'kelompok_akun_id']);
    }

    /**
     * {@inheritdoc}
     * @return \app\models\query\TbAkunQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\query\TbAkunQuery(get_called_class());
    }
}
