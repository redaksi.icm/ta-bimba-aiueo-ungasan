<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%jurnal_detail}}".
 *
 * @property int $id
 * @property int $jurnal_id
 * @property int $item
 * @property int $akun_id
 * @property int $debit_kredit
 * @property int $nilai
 *
 * @property TbAkun $akun
 * @property Jurnal $jurnal
 */
class JurnalDetail extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%jurnal_detail}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['jurnal_id', 'item', 'akun_id', 'debit_kredit', 'nilai'], 'integer'],
            [['akun_id'], 'exist', 'skipOnError' => true, 'targetClass' => TbAkun::className(), 'targetAttribute' => ['akun_id' => 'id']],
            [['jurnal_id'], 'exist', 'skipOnError' => true, 'targetClass' => Jurnal::className(), 'targetAttribute' => ['jurnal_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'jurnal_id' => Yii::t('app', 'Jurnal ID'),
            'item' => Yii::t('app', 'Item'),
            'akun_id' => Yii::t('app', 'Akun ID'),
            'debit_kredit' => Yii::t('app', 'Debit Kredit'),
            'nilai' => Yii::t('app', 'Nilai'),
        ];
    }

    /**
     * Gets query for [[Akun]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAkun()
    {
        return $this->hasOne(TbAkun::className(), ['id' => 'akun_id']);
    }

    /**
     * Gets query for [[Jurnal]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getJurnal()
    {
        return $this->hasOne(Jurnal::className(), ['id' => 'jurnal_id']);
    }
}
