<?php

namespace app\models;

use Yii;
use yii\base\NotSupportedException;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * User model
 *
 * @property integer $id
 * @property string $username
 * @property string $password_hash
 * @property string $password_raw
 * @property integer $status
 * @property integer $role
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $logged_at
 * @property string $password write-only password
 */

class User extends ActiveRecord implements IdentityInterface
{

    const STATUS_DELETED = 0;
    const STATUS_INACTIVE = 10;
    const STATUS_ACTIVE = 20;

    const ROLE_KEPALA_AKADEMIK = 10;
    const ROLE_PIMPINAN = 20;
    const ROLE_FINANCIAL_ACCOUNT = 30;
    const ROLE_ACCOUNTING = 40;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['status', 'default', 'value' => self::STATUS_INACTIVE],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_INACTIVE, self::STATUS_DELETED]],
            ['role', 'default', 'value' => self::ROLE_ACCOUNTING],
            ['role', 'in', 'range' => [self::ROLE_KEPALA_AKADEMIK, self::ROLE_PIMPINAN, self::ROLE_FINANCIAL_ACCOUNT, self::ROLE_ACCOUNTING]],
        ];
    }

    public function getIsUserKepalaAkademik()
    {
        return $this->checkUserAccess($this->role, self::ROLE_KEPALA_AKADEMIK);
    }

    public function getIsUserPimpinan()
    {
        return $this->checkUserAccess($this->role, self::ROLE_PIMPINAN);
    }

    public function getIsUserFinancialAcc()
    {
        return $this->checkUserAccess($this->role, self::ROLE_FINANCIAL_ACCOUNT);
    }

    public function getIsUserAccounting()
    {
        return $this->checkUserAccess($this->role, self::ROLE_ACCOUNTING);
    }

    private function checkUserAccess($username, $role)
    {
        if (static::findOne(['username' => $username, 'role' => $role])){
            return true;
        }
        return false;
    }


    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        return true;
//                throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    public function getAuthKey()
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }
}
