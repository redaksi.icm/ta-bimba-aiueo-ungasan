<?php

namespace app\models\query;

/**
 * This is the ActiveQuery class for [[\app\models\TbMurid]].
 *
 * @see \app\models\TbMurid
 */
class TbMuridQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return \app\models\TbMurid[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return \app\models\TbMurid|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
