<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%jurnal}}".
 *
 * @property int $id
 * @property string $no
 * @property string $tgl
 * @property int $f_id
 * @property string $keterangan
 * @property int $login_id
 * @property string $waktu_post
 *
 * @property JurnalDetail[] $jurnalDetails
 */
class Jurnal extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%jurnal}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['tgl', 'waktu_post'], 'safe'],
            [['f_id', 'login_id'], 'integer'],
            [['keterangan', 'waktu_post'], 'required'],
            [['keterangan'], 'string'],
            [['no'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'no' => Yii::t('app', 'No'),
            'tgl' => Yii::t('app', 'Tgl'),
            'f_id' => Yii::t('app', 'F ID'),
            'keterangan' => Yii::t('app', 'Keterangan'),
            'login_id' => Yii::t('app', 'Login ID'),
            'waktu_post' => Yii::t('app', 'Waktu Post'),
        ];
    }

    /**
     * Gets query for [[JurnalDetails]].
     *
     * @return \yii\db\ActiveQuery|\app\models\query\JurnalDetailQuery
     */
    public function getJurnalDetails()
    {
        return $this->hasMany(JurnalDetail::className(), ['jurnal_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return \app\models\query\JurnalQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\query\JurnalQuery(get_called_class());
    }
}
